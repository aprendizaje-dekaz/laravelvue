<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CREATE</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
</head>
<body>
 <div class="container">
    <form class="form-horizontal" method="POST" action= "/companies" >
        @csrf
        <fieldset>
        
        <!-- Form Name -->
        <legend>Crear empresa</legend>
        
        <!-- Text input-->
        <div class="field">
          <label class="label" for="title">title</label>
          <div class="control">
            <input id="title" name="title" type="text" placeholder="title" class="input is-medium">
            <p class="help">help</p>
          </div>
        </div>
        
        <!-- Text input-->
        <div class="field">
          <label class="label" for="body">body</label>
          <div class="control">
            <input id="body" name="body" type="text" placeholder="body" class="input is-medium">
            <p class="help">help</p>
          </div>
        </div>
        
        <!-- Button -->
        <div class="field">
          <label class="label" for="singlebutton-0">Single Button</label>
          <div class="control">
            <button id="singlebutton-0"  class="button is-primary">enviar</button>
            {{-- <input type="submit" value="submit"> --}}
          </div>
        </div>
        
        </fieldset>
        </form>
    </div>
</body>
</html>